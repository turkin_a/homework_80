DROP DATABASE IF EXISTS `db`;
CREATE DATABASE `db`;

CREATE TABLE `db`.`places` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT
);

CREATE TABLE `db`.`categories` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT
);

CREATE TABLE `db`.`assets` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `category_id` INT NOT NULL,
    `place_id` INT NOT NULL,
    `description` TEXT,
    `image` VARCHAR(255),
    INDEX `FK_category_idx` (`category_id`),
    INDEX `FK_place_idx` (`place_id`),
    CONSTRAINT `FK_category`
		FOREIGN KEY (`category_id`)
        REFERENCES `db`.`categories` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    CONSTRAINT `FK_place`
		FOREIGN KEY (`place_id`)
        REFERENCES `db`.`places` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);