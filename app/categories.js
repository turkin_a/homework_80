const express = require('express');

const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    db.query('SELECT * FROM `categories`', function (error, results) {
      if (error) throw error;

      res.send(results);
    });
  });

  router.post('/', (req, res) => {
    const category = req.body;

    db.query('INSERT INTO `db`.`categories` (`name`, `description`)' +
    `VALUES ('${category.name}', '${category.description}')`,
    (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  return router;
};

module.exports = createRouter;