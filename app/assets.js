const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage: storage});
const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    db.query('SELECT * FROM `assets`', (error, results) => {
      if (error) throw error;

      res.send(results);
    });
  });

  router.get('/:id', (req, res) => {
    db.query('SELECT * FROM `assets` WHERE id=' + req.params.id, (error, results) => {
      if (error) throw error;

      res.send(results);
    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const asset = req.body;
    if (req.file) {
      post.image = req.file.filename;
    }

    db.query('INSERT INTO `db`.`assets` (`name`, `category_id`, `place_id`, `description`, `image`)' +
    `VALUES ('${asset.name}', '${asset.categoryId}', '${asset.placeId}', '${asset.description}, '${asset.image}')`,
    (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  return router;
};

module.exports = createRouter;