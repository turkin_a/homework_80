const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const categories = require('./app/categories');
const places = require('./app/places');
const assets = require('./app/assets');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'wbnajhev',
  database : 'db'
});

connection.connect((err) => {
  if (err) throw err;

  app.use('/categories', categories(connection));
  app.use('/places', places(connection));
  app.use('/assets', assets(connection));

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});